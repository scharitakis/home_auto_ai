const PouchDB = require('pouchDB');
const async = require('async'); 

var repl = require('repl')
let instance = null;

class PoucDBContext{
	constructor(params){
		 if(!instance){
		 	this.name = params.name || 'test'
			this.remotes=params.remotes || [{name:'remote1',url:'http://127.0.0.1:5984/'+this.name}];
            this.db = new PouchDB(this.name);
            this.syncRemotes = {};
            this.syncHandlers = {};
            if(this.remotes.length){
            	this.remotes.forEach((r)=>{
            		this.syncRemotes[r.name] = new PouchDB(r.url)
            	})
				let remotesNames = Object.keys(this.syncRemotes);
				remotesNames.forEach((name)=>{
					this.addReplication(name);
				})
            }
            
            instance = this;

        	   
        }

        return instance;
		
	}

	addReplication(remoteName){
		var remoteDB = this.syncRemotes[remoteName];
		var syncHandler = this.db.sync(remoteDB, {
		  live: true,
		  retry: true
		});
		syncHandler.on('complete',(info)=>{
			// need to remove from syncHandlers
			console.log(info);
			console.log(remoteName)
			delete this.syncHandlers[remoteName]
			
		})
		
		syncHandler.on('change',(info)=>{
			// handle change	
			console.log('syncHandler change',JSON.stringify(info))
		})
		
		syncHandler.on('paused',(err)=>{
			// replication paused (e.g. replication up to date, user went offline)	
			console.log('syncHandler paused',err)			
		})
		
		syncHandler.on('active',()=>{
			// replicate resumed (e.g. new changes replicating, user went back online)	
			console.log('syncHandler active')			
		})
		
		syncHandler.on('denied',(err)=>{
			 // a document failed to replicate (e.g. due to permissions)	
			console.log('syncHandler denied',err)				 
		})
		
		syncHandler.on('error',(err)=>{
			// handle error
			console.log('syncHandler error',err)	
			
		})
		
		this.syncHandlers[remoteName] = syncHandler;


	}

	cancelReplication(remoteName){
		this.syncHandlers[remoteName].cancel();
	}

	addRemote(remote){
		/*{name:'',url:''}*/
		this.remotes.push(remote)
		this.syncRemotes[remote.name] = new PouchDB(remote.url)
  
	}

	getRemotes(){
		return this.remotes
	}
	
	getSyncRemotes(){
		return this.syncRemotes
	}
	
	getSyncHandlers(){
		return this.syncHandlers
	}

	put(doc){
		return new Promise((resolve, reject)=>{
			let id = doc._id
			this.db.get(id).then((res)=>{
				//update
				doc._rev = res._rev				
				resolve(this.db.put(doc))

			}).catch((err)=>{
				if(err.name=="not_found"){
					//create new
					resolve(this.db.put(doc))
				}else{
					reject(err)
				}
			})
		})
	}

	get(id,params){
		var p = (params!=undefined)?params:{};
		return this.db.get(id,p);
	}
	
	remove(id){
		return new Promise((resolve, reject)=>{
			this.db.get(id).then((res)=>{
				resolve(this.db.remove(res))
			}).catch((err)=>{
				reject(err)
			})
		})
	}
	
	bulk(docs){
		return this.db.bulkDocs(docs)
	}
	
	allDocs(params){
		let p = (params!=undefined)?params:{include_docs: true};
		return this.db.allDocs(p);
	}

	
	/*
	var ddoc = {
		_id: '_design/my_index',
		views: {
			by_name: {
				map: function (doc) { emit(doc.name); }.toString(),
				reduce:''
			}
		}
	};
	*/
	putView(ddoc){
		return new Promise((resolve, reject)=>{
			if( ddoc._id==undefined || ddoc.views==undefined){
				reject('params error')
				return
			}
			// save it
			resolve(this.put(ddoc));
		});
	}

	query(ddocname,params){
		let p =(params!=undefined)?params:{include_docs: true};
		return this.db.query(ddocname,p)
	}
	
	viewCleanup(){
		return this.db.viewCleanup()
	}


}
/*
var d = new PoucDBContext({})
var replServer = repl.start({
	prompt: "home-auto > ",
});

replServer.context.db = d;*/

module.exports = PoucDBContext;