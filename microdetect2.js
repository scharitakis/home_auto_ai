
var micStream = require('node-record-lpcm16');
const {Detector, Models} = require('snowboy');
var fs = require('fs');
var stream = require('stream');
var request = require('request');
var beep = require('beepbeep');
var say = require('say');
var wav = require('wav');




var myfile  = null;
var streamMic = null;

function startStream(){
	myfile  = null;
	micRecStream = null;
	streamMic = null;

	streamMic  = micStream.start({threshold:0,verbose:true})
	
	//myfile = fs.createWriteStream(filename+'.wav')
	var witToken = "2XQOXTT3HDOXGRR7BZP3Q5QMKFL4JC2D"

	streamMic.pipe(request.post(
      {
      'url'     : 'https://api.wit.ai/speech?client=chromium&lang=en-us&output=json',
      'headers' : {
        'Accept'        : 'application/vnd.wit.20160202+json',
        'Authorization' : 'Bearer ' + witToken,
        'Content-Type'  : 'audio/wav'
      }},
      function optionalCallback(err, httpResponse, body) {
        if (err) {
          return console.error('upload failed:', err);
        }
        console.log('Upload successful!  Server responded with:', body);
      }))

	
	streamMic.on('end',function(data){
		console.log('ending mic')

	})
	

}




class Stb {
  constructor(){
    this.streamMic =null

  }

  createDetector(){
  	var models = new Models();

   	models.add({
      file: 'resources/alexa.umdl',
      sensitivity: '0.5',
      hotwords : 'snowboy'
    });

  	var detector = new Detector({
      resource: "resources/common.res",
      models: models,
      audioGain: 2.0
    });

    detector.on('silence', ()=>{
     console.log('silence');
    });

    detector.on('sound',  () =>{
      console.log('sound');
      //this.recordWav()
    });

    detector.on('error', function () {
      console.log('error');
    });

    detector.on('hotword', (index, hotword)=> {
      console.log('hotword', index, hotword);
      this.recordWav()
      /*  
      this.stopReco();
      setTimeout(()=>{
      	beep()
		    this.sendReco()
      },100)

      setTimeout(()=>{
      	//console.log('stop rec ======')
		    beep()
		    this.stopReco()
      },5000)
      setTimeout(()=>{
      	console.log('start detect======')
		    this.start()
      },6100)
      */
     
    
     });

    return detector
  }

  recordWav(){
    var wavFile = fs.createWriteStream('testwav.wav')
   

    this.streamMic.pipe(wavFile)

 
    setTimeout(()=>{
      this.streamMic.emit('end')
    },4000)
  }

  stopReco(){
  	micStream.stop()
  	this.streamMic.emit('end')
	this.streamMic.unpipe()

  }

  processCmd(text){
  	
  	switch(text){
  		case "one":
  			console.log('ooooo')
  			break;
  		case "what time is it":
  			var date =new Date();
  			var time = (date.getHours()>12?date.getHours()-12:date.getHours()) + ' and ' + date.getMinutes() + ' minutes';
  			say.speak('The time is' +time);
  			break;
  		case "turn off the lights":
  			say.speak('turning of the lights master');
  			break;
  		default:
  			say.speak('Did not understand master');
  	}
  }

  sendReco(){
  	this.streamMic =null
    this.streamMic = micStream.start({ threshold: 0, verbose : true})
  	var witToken = "2XQOXTT3HDOXGRR7BZP3Q5QMKFL4JC2D"
  	// var myfile = fs.createWriteStream('alexa1.wav',{encoding:'binary'})
  	// this.streamMic.pipe(myfile)

  	//this.streamMic.pipe(myfile)
	this.streamMic.pipe(request.post(
      {
      'url'     : 'https://api.wit.ai/speech?client=chromium&lang=en-us&output=json',
      'headers' : {
        'Accept'        : 'application/vnd.wit.20160202+json',
        'Authorization' : 'Bearer ' + witToken,
        'Content-Type'  : 'audio/wav'
      }},
      (err, httpResponse, body) =>{
        if (err) {
          return console.error('upload failed:', err);
        }
        console.log('Upload successful!  Server responded with:', body);
        var res = JSON.parse(body)
        this.processCmd(res._text)
      }))
  }

  start(){ 
  	this.streamMic =null
    this.streamMic = micStream.start({ threshold: 0, verbose : true})
    this.detector = this.createDetector();
    this.streamMic.pipe(this.detector)

  }

}

var app = new Stb();

app.start()

/*
setTimeout(function(){
	startStream('test_wav1');
},200);

setTimeout(function(){
	console.log('end test1')
	micStream.stop()
	//streamMic.emit('end')
	//streamMic.unpipe()
	//myfile.emit('end')
	streamMic.emit('end')
	streamMic.unpipe()
	//myfile.emit('end')

},3000);



setTimeout(function(){
	startStream('test_wav2');
},4000);

setTimeout(function(){
	console.log('end test2')
	micStream.stop()
	//streamMic.emit('end')
	//streamMic.unpipe()
	//myfile.emit('end')
	streamMic.emit('end')
	streamMic.unpipe()
	//myfile.emit('end')

},9000);

setTimeout(function(){
	console.log('finished')
},10000);

*/
