var PORT = 80;
var BROADCAST_ADDR = "255.255.255.255";
var dgram = require('dgram'); 
var server = dgram.createSocket("udp4"); 
var crypto = require('crypto');
//aes-128-cbc



var key = [0x09, 0x76, 0x28, 0x34, 0x3f, 0xe9, 0x9e, 0x23, 0x76, 0x5c, 0x15, 0x13, 0xac, 0xcf, 0x8b, 0x02]
var iv = [0x56, 0x2e, 0x17, 0x99, 0x6d, 0x09, 0x3d, 0x28, 0xdd, 0xb3, 0xba, 0x69, 0x5a, 0x2e, 0x6f, 0x58]
var id = [0, 0, 0, 0]

var buffkey = new Buffer(key);
var buffiv = new Buffer(iv);


function decrypt(buffer){
	var decipher = crypto.createDecipheriv('aes-128-cbc', buffkey, buffiv)
	var dec = Buffer.concat([decipher.update(buffer) , decipher.final()]);
	return dec;

}


function encrypt(buffer){
	var cipher = crypto.createCipheriv('aes-128-cbc', buffkey, buffiv)
 	var crypted = Buffer.concat([cipher.update(buffer),cipher.final()]);
 	return crypted;
}


var message = new Buffer([0x0, 0x0, 0x0, 0x0,
     0x0, 0x0, 0x0, 0x0, 0x02, 0x0, 0x0, 0x0, 0xe0, 0x07, 0x29, 0x15, 0x10,
     0x02, 0x0d, 0x0c, 0x0, 0x0, 0x0,
     0x0, 0xc0, 0xa8, 0x01, 0x09, 0xea,
     0xd3, 0x0, 0x0, 0x36, 0xc3, 0x0,
     0x0, 0x0, 0x0, 0x06, 0x0, 0x0,
     0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0]);



function testEncDec(){
	var enc = encrypt(message);
	console.log('enc',enc);

	console.log('dec',decrypt(enc));
}


server.bind(function() {
    server.setBroadcast(true);
    //setInterval(broadcastNew, 3000);
    //broadcastNew();
});

function broadcastNew() {
    var message = new Buffer([0x0, 0x0, 0x0, 0x0,
     0x0, 0x0, 0x0, 0x0, 02, 0x0, 0x0, 0x0, 0xe0, 0x07, 0x29, 0x15, 0x10,
     02, 0x0d, 0x0c, 0x0, 0x0, 0x0,
     0x0, 0xc0, 0xa8, 0x01, 0x09, 0xea,
     0xd3, 0x0, 0x0, 36, 0xc3, 0x0,
     0x0, 0x0, 0x0, 0x06, 0x0, 0x0,
     0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0]);

/*
<Buffer 00 00 00 00 00 00 00 00 02 00 00 00 e0 07 29 15 10 02 0d 0c 00 00 00 00 c0 a8 01 09 ea d3 00 00 36 c3 00 00 00 00 06 00 00 00 00 00 00 00 00 00>
*/

    server.send(message, 0, message.length, PORT, BROADCAST_ADDR, function() {
        console.log("Sent '" + message + "'");
    });
}

server.on('listening', function () {
    var address = server.address();
    console.log('UDP Client listening on ' + address.address + ":" + address.port);
});

server.on('message', function (message, rinfo) {
    console.log('Message from: ' + rinfo.address + ':' + rinfo.port + ' - ' + message);
    console.log(message)
    console.log(rinfo);
});


class discover{
	constructor(port,broadcastAddr){
		this.PORT = 80;
		this.BROADCAST_ADDR =broadcastAddr
		this.server = dgram.createSocket("udp4"); 

	}

	start(){
		this.server.bind(function() {
	    	server.setBroadcast(true);
	    //setInterval(broadcastNew, 3000);
	    //broadcastNew();
		});

		this.server.on('listening', this.onlistening);

		this.server.on('message', this.onmessage);
	}

	onmessage(message, rinfo) {
		    console.log('Message from: ' + rinfo.address + ':' + rinfo.port + ' - ' + message);
		    console.log(message)
		    console.log(rinfo);
	});

	onlistening(){
		var address = this.server.address();
	    console.log('UDP Client listening on ' + address.address + ":" + address.port);
	}

	sendDiscover(){
		var message = new Buffer([0x0, 0x0, 0x0, 0x0,
	     0x0, 0x0, 0x0, 0x0, 02, 0x0, 0x0, 0x0, 0xe0, 0x07, 0x29, 0x15, 0x10,
	     02, 0x0d, 0x0c, 0x0, 0x0, 0x0,
	     0x0, 0xc0, 0xa8, 0x01, 0x09, 0xea,
	     0xd3, 0x0, 0x0, 36, 0xc3, 0x0,
	     0x0, 0x0, 0x0, 0x06, 0x0, 0x0,
	     0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0]);

		this,server.send(message, 0, message.length, PORT, BROADCAST_ADDR, function() {
        	console.log("Sent '" + message + "'");
    	});
	}

}



class Device {
	constructor(host,mac){
		this.host = host;
		this.mac = mac;
		this.count = 0x01;
		this.key = [0x09, 0x76, 0x28, 0x34, 0x3f, 0xe9, 0x9e, 0x23, 0x76, 0x5c, 0x15, 0x13, 0xac, 0xcf, 0x8b, 0x02]
		this.iv = [0x56, 0x2e, 0x17, 0x99, 0x6d, 0x09, 0x3d, 0x28, 0xdd, 0xb3, 0xba, 0x69, 0x5a, 0x2e, 0x6f, 0x58]
		this.id = [0, 0, 0, 0]

	}

	auth(){
		var payload = new Array(80);
		payload[0] = 0x00
		payload[1] = 0x00
		payload[2] = 0x00
		payload[3] = 0x00
		payload[4] = 0x31
	    payload[5] = 0x31
	    payload[6] = 0x31
	    payload[7] = 0x31
	    payload[8] = 0x31
	    payload[9] = 0x31
	    payload[10] = 0x31
	    payload[11] = 0x31
	    payload[12] = 0x31
	    payload[13] = 0x31
	    payload[14] = 0x31
	    payload[15] = 0x31
	    payload[16] = 0x31
	    payload[17] = 0x31
	    payload[18] = 0x31
	    
	    payload[19] = 0x01

	    payload[20] = 0x00
	    payload[21] = 0x00
	    payload[22] = 0x00
	    payload[23] = 0x00
	    payload[24] = 0x00
	    payload[25] = 0x00
	    payload[26] = 0x00
	    payload[27] = 0x00
	    payload[28] = 0x00
	    payload[29] = 0x00
	    payload[30] = 0x00
	    payload[31] = 0x00
	    payload[32] = 0x00
	    payload[33] = 0x00
	    payload[34] = 0x00
	    payload[35] = 0x00
	    payload[36] = 0x00
	    payload[37] = 0x00
	    payload[38] = 0x00
	    payload[39] = 0x00
	    payload[40] = 0x00
	    payload[41] = 0x00
	    payload[42] = 0x00
	    payload[43] = 0x00
	    payload[44] = 0x00



	    //payload[30] = 0x01
	    payload[45] = 0x01
	    
	    payload[48] = //ord('T')
	    payload[49] = //ord('e')
	    payload[50] = //ord('s')
	    payload[51] = //ord('t')
	    payload[52] = //ord(' ')
	    payload[53] = //ord(' ')
	    payload[54] = //ord('1')

	}

	send_packet(command,payload){
		var packet = new Array(56);
		packet[0] = 0x5a
	    packet[1] = 0xa5
	    packet[2] = 0xaa
	    packet[3] = 0x55
	    packet[4] = 0x5a
	    packet[5] = 0xa5
	    packet[6] = 0xaa
	    packet[7] = 0x55

	    packet[8] = 0x00
	    packet[9] = 0x00
	    packet[10] = 0x00
	    packet[11] = 0x00
	    packet[12] = 0x00
	    packet[13] = 0x00
	    packet[14] = 0x00
	    packet[15] = 0x00
	    packet[16] = 0x00
	    packet[17] = 0x00
	    packet[18] = 0x00
	    packet[19] = 0x00
	    packet[20] = 0x00
	    packet[21] = 0x00
	    packet[22] = 0x00
	    packet[23] = 0x00
	    packet[24] = 0x00
	    packet[25] = 0x00
	    packet[26] = 0x00
	    packet[27] = 0x00
	    packet[28] = 0x00
	    packet[29] = 0x00
	    packet[30] = 0x00
	    packet[31] = 0x00

	    packet[32] = checksum
	    packet[33] = checksum
	    packet[34] = 0x00
	    packet[35] = 0x00

	    packet[36] = 0x2a
	    packet[37] = 0x27
	    packet[38] = command

	    packet[40] = this.count & 0xff
	    packet[41] = this.count >> 8
	    packet[42] = this.mac[0]
	    packet[43] = this.mac[1]
	    packet[44] = this.mac[2]
	    packet[45] = this.mac[3]
	    packet[46] = this.mac[4]
	    packet[47] = this.mac[5]
	    packet[48] = this.id[0]
	    packet[49] = this.id[1]
	    packet[50] = this.id[2]
	    packet[51] = this.id[3]

	    packet[52] = checksum
	    packet[53] = checksum
	    packet[54] = 0x00
	    packet[55] = 0x00

	    //add payload after this

	    var checksum = 0xbeaf;
	    for(var i=0; i<payload.length;i++){
	    	checksum += payload[i];
	    	checksum = checksum & 0xffff;
	    } 

	    var buffPayload = new Buffer(payload);


	}

}

class Rm extends Device{
	constructor(host,mac){
		super(host, mac);
		this.type = "RM";
	}


	
}