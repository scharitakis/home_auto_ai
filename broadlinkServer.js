var dgram = require('dgram'); 
var server = dgram.createSocket("udp4"); 
var crypto = require('crypto');
var moment = require('moment');
var EventEmitter = require('events');

class DiscoverServer extends EventEmitter{
	constructor(port,broadcastAddr){
		super();
		this.PORT = port ||80;
		this.BROADCAST_ADDR = broadcastAddr || "255.255.255.255";
		this.server = dgram.createSocket("udp4"); 
		this.devices = []
		

	}

	start(){
		this.server.bind(()=>{
			this.serverAddress = this.server.address();
			console.log(this.serverAddress );
	    	this.server.setBroadcast(true);
		});

		this.server.on('listening', () =>{this.onlistening()});
		this.server.on('message', (message, rinfo)=>{this.onmessage(message,rinfo)});
	}

	onmessage(message, rinfo) {
			
		    //console.log('Message from: ' + rinfo.address + ':' + rinfo.port + ' - ' + message);
		    console.log("Device Found "+ rinfo.address )
		    console.log(message)
		    console.log(rinfo);
		    this.createDevice(message, rinfo)
	}

	onlistening(){
		this.serverAddress = this.server.address();
	    console.log('UDP Client listening on ' + this.serverAddress.address + ":" + this.serverAddress.port);
	    this.discover();
	}

	discover(timeout){

		var date = moment()
		var address = this.serverAddress.address.split('.');
		var port = this.serverAddress.port
		var packet= new Array(48);
		/*packet[0]=0x0;
		packet[1]=0x0;
		packet[2]=0x0;
		packet[3]=0x0;
		packet[4]=0x0;
		packet[5]=0x0;
		packet[6]=0x0;
		packet[7]=0x0;*/

		packet[8]=0x02; // timezone		Current offset from GMT as a little-endian 32 bit integer
		// packet[9]=0x0; 
		// packet[10]=0x0; 
		// packet[11]=0x0; 

		packet[12]=date.year() & 0xff; //Current year as a little-endian 16 bit integer
		packet[13]=date.year() >> 8; 

		packet[14]=date.minutes()  // Current number of minutes past the hour

		packet[15]=date.hour(); //Current number of hours past midnight

		packet[16]=parseInt(date.year().toString().slice(2)); //Current number of years past the century

		packet[17]=date.day(); //Current day of the week (Monday = 0, Tuesday = 1, etc)

		packet[18]=date.date(); //Current day in month

		packet[19]=date.month(); //Current month

		// packet[20]=0x0;
		// packet[21]=0x0;
		// packet[22]=0x0;
		// packet[23]=0x0;

		packet[24]=address[0]; //ip address
		packet[25]=address[1];	//ip address
		packet[26]=address[2];	//ip address
		packet[27]=address[3];	//ip address

		packet[28]=port & 0xff; //Source port as a little-endian 16 bit integer
		packet[29]=port >> 8;

		// packet[30]=0x0;
		// packet[31]=0x0;

		//packet[32]=0x0; //Checksum as a little-endian 16 bit integer
		//packet[33]=0x0;

		// packet[34]=0x0;
		// packet[35]=0x0;
		// packet[36]=0x0;
		// packet[37]=0x0;
		packet[38]=0x06; //6
		
		// packet[39]=0x0;
		// packet[40]=0x0;
		// packet[41]=0x0;
		// packet[42]=0x0;
		// packet[43]=0x0;
		// packet[44]=0x0;
		// packet[45]=0x0;
		// packet[46]=0x0;
		// packet[47]=0x0;

		var checksum = 0xbeaf

		for(var i=0;i<packet.length;i++){
	    	checksum += packet[i]
		}
		checksum = checksum & 0xffff
		packet[32] = checksum & 0xff //Checksum as a little-endian 16 bit integer
		packet[33] = checksum >> 8  //Checksum as a little-endian 16 bit integer

		var buffPacket = new Buffer(packet);

		this.server.send(buffPacket, 0, buffPacket.length, this.PORT, this.BROADCAST_ADDR, function() {
        	console.log("Sent buffPacket");
        	console.log(buffPacket);
    	});
	}

	createDevice(message, rinfo){
		//Bytes 0x3a-0x40 of any unicast response will contain the MAC address of the target device.
			//58-64 mac
			//52-53 device Type
		
		    var device = message[52] | message[53] << 8
		  	var deviceToHex = device.toString(16);
		  	var mac  = message.slice(58,64)
	  	  	console.log('mac',mac) // mac
		    console.log(device)
		    console.log('device',deviceToHex) // device

		switch (device){
			case 0: 	// SP1
				break;
			case 10001: 	// SP2
				break;
			case 10009:  //Honeywell SP2
			case 31001:
			case 10010:
			case 31002:
				break;
			case 10016: 	//SPMini
				break;
			case 30014: 	//SP3
				break;
			case 10024: 	//SPMini2
				break;
			case 10035:
			case 10046: 	//OEM branded SPMini
				break;
			case 10038: 	//SPMiniPlus
				break;
			case 10002: 	//RM2
				break;
			case 10039:  //RM Mini
				console.log("Rm Mini")
				break;
			case 10045: 	//RM Pro Phicomm
				break;
			case 10115: 	//RM2 Home Plus
				break;
			case 10108: 	//RM2 Home Plus GDT
				break;
			case 10026 : 	//RM2 Pro Plus
				console.log("RM2 Pro Plus")
				break;
			case 10119: 	//RM2 Pro Plus2
				break;
			case 10123: 	//RM2 Pro Plus BL
				break;
			case 10127: 	//RM Mini Shate
				break;
			case 10004: 	//A1
				break;

		}

		var host = rinfo.address
		var device = new Device(host,mac);
		this.devices.push(device);
		this.emit('test');
	}



}

class Device {
	constructor(host,mac)
	{
		this.host = host;
		this.mac = mac;
		this.count = 0x01;
		this.key = new Buffer([0x09, 0x76, 0x28, 0x34, 0x3f, 0xe9, 0x9e, 0x23, 0x76, 0x5c, 0x15, 0x13, 0xac, 0xcf, 0x8b, 0x02])
		this.iv = new Buffer([0x56, 0x2e, 0x17, 0x99, 0x6d, 0x09, 0x3d, 0x28, 0xdd, 0xb3, 0xba, 0x69, 0x5a, 0x2e, 0x6f, 0x58])
		this.id = [0, 0, 0, 0]
		this.count = 0xffff

	}

	decrypt(buffer){
		var buffkey = this.key
		var buffiv = this.iv
		var decipher = crypto.createDecipheriv('aes-128-cbc', buffkey, buffiv)
		var dec = Buffer.concat([decipher.update(buffer) , decipher.final()]);
		return dec;
	}


 	encrypt(buffer){
 		var buffkey = this.key
		var buffiv = this.iv
		var cipher = crypto.createCipheriv('aes-128-cbc', buffkey, buffiv)
 		var crypted = Buffer.concat([cipher.update(buffer),cipher.final()]);
 		return crypted;
	}

	send_packet(command,payload){
		this.count = (this.count + 1) & 0xffff

		var packet = new Array(56);
		packet[0] = 0x5a
	    packet[1] = 0xa5
	    packet[2] = 0xaa
	    packet[3] = 0x55
	    packet[4] = 0x5a
	    packet[5] = 0xa5
	    packet[6] = 0xaa
	    packet[7] = 0x55

	    // packet[8] = 0x00
	    // packet[9] = 0x00
	    // packet[10] = 0x00
	    // packet[11] = 0x00
	    // packet[12] = 0x00
	    // packet[13] = 0x00
	    // packet[14] = 0x00
	    // packet[15] = 0x00
	    // packet[16] = 0x00
	    // packet[17] = 0x00
	    // packet[18] = 0x00
	    // packet[19] = 0x00
	    // packet[20] = 0x00
	    // packet[21] = 0x00
	    // packet[22] = 0x00
	    // packet[23] = 0x00
	    // packet[24] = 0x00
	    // packet[25] = 0x00
	    // packet[26] = 0x00
	    // packet[27] = 0x00
	    // packet[28] = 0x00
	    // packet[29] = 0x00
	    // packet[30] = 0x00
	    // packet[31] = 0x00

	    //packet[32] = checksum //Checksum of full packet as a little-endian 16 bit integer 0x20-0x21
	    //packet[33] = checksum
	    // packet[34] = 0x00
	    // packet[35] = 0x00

	    packet[36] = 0x2a
	    packet[37] = 0x27
	    packet[38] = command //Command code as a little-endian 16 bit integer

	    packet[40] = this.count & 0xff //Packet count as a little-endian 16 bit integer
	    packet[41] = this.count >> 8
	    packet[42] = this.mac[0]
	    packet[43] = this.mac[1]
	    packet[44] = this.mac[2]
	    packet[45] = this.mac[3]
	    packet[46] = this.mac[4]
	    packet[47] = this.mac[5]
	    packet[48] = this.id[0]
	    packet[49] = this.id[1]
	    packet[50] = this.id[2]
	    packet[51] = this.id[3]

	    //packet[52] = checksum  //Checksum of packet header as a little-endian 16 bit integer 0x34-0x35
	    //packet[53] = checksum
	    // packet[54] = 0x00
	    // packet[55] = 0x00

	    //add payload after this

	    var checksum = 0xbeaf;
	    for(var i=0; i<payload.length;i++){
	    	checksum += payload[i];
	    	checksum = checksum & 0xffff;
	    } 


	    var buffPayload = new Buffer(payload);
	    var encPayload = this.encrypt(buffPayload);

	    packet[52] = checksum & 0xff
	    packet[53] = checksum >> 8

	    for(var i=0; i<encPayload.length;i++){
      		packet.push(encPayload[i])
      	}

	    checksum = 0xbeaf
   		for(var i=0; i<packet.length;i++){
	    	checksum += packet[i];
	    	checksum = checksum & 0xffff;
	    } 

	    packet[32] = checksum & 0xff
    	packet[33] = checksum >> 8

    	//Need to Send the packet



	}

	auth(){
		var payload = new Array(80);
		payload[4] = 0x31
	    payload[5] = 0x31
	    payload[6] = 0x31
	    payload[7] = 0x31
	    payload[8] = 0x31
	    payload[9] = 0x31
	    payload[10] = 0x31
	    payload[11] = 0x31
	    payload[12] = 0x31
	    payload[13] = 0x31
	    payload[14] = 0x31
	    payload[15] = 0x31
	    payload[16] = 0x31
	    payload[17] = 0x31
	    payload[18] = 0x31    
	    
	    payload[30] = 0x01
	    payload[45] = 0x01
	    
	    payload[48] = 0x00//ord('T')
	    payload[49] = 0x00//ord('e')
	    payload[50] = 0x00//ord('s')
	    payload[51] = 0x00//ord('t')
	    payload[52] = 0x00//ord(' ')
	    payload[53] = 0x00//ord(' ')
	    payload[54] = 0x00//ord('1')

	    this.send_packet(0x65,payload)

	}

	
}

class RM extends Device{
	constructor(){
		super();

	}

	check_data(){
		var packet = new Array(16);
		packet[0] = 4
		this.send_packet(0x6a,packet)
	}

	send_data(data){
		var packet = [0x02, 0x00, 0x00, 0x00]
		packet += data
		this.send_packet(0x6a,packet)
	}

	enter_learning(){
		var packet = new Array(16);
		packet[0] = 3
		this.send_packet(0x6a,packet)
	}

	check_temperature(){
		var packet = new Array(16);
		packet[0] = 3
		this.send_packet(0x6a,packet)
	}

}


var server = new DiscoverServer()

server.on("test",function(){
	console.log('test');
})

server.start();

//server.discover();

