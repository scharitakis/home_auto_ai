//adapt mycroft

var PythonShell = require('python-shell');
var EventEmitter = require('events');


class Adapt extends EventEmitter{
	constructor(){
		super();
		this.options ={
			mode:'text',
			scriptPath: './adapt/examples/',
		}
		this.script = 'multi_domain_intent_parser1.py'
	}

	parse(text){
		this.options.args = [text];
		PythonShell.run(this.script, this.options, (err, results) =>{
		  if (err) throw err;
		  this.emit('data',JSON.parse(results))
		  //console.log(JSON.parse(results));
		});
	}


}


//var d = new Adapt();
//d.on('data',function(data){console.log(data)})
//d.parse("please turn off the tv and the lights");
module.exports = Adapt;


