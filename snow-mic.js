const record = require('node-record-lpcm16');
const {Detector, Models} = require('snowboy');
const request = require('request');
var stream = require('stream');
var util = require('util');
var fs = require('fs');


class RecognitionAI {
  constructor(){
    this.recStream = new stream.PassThrough()

    

    //this.detStream = detStream
    this.sendIt = false;
    this.witToken = "2XQOXTT3HDOXGRR7BZP3Q5QMKFL4JC2D"
    //this.liner = new stream.Transform();
    this.wavWritter = fs.createWriteStream('mytest3.wav',{ encoding: 'binary' });

  }

  stop(stream){
    console.log('ending stream');
    stream.unpipe(this.recStream)
    this.recStream.end();
    
    //this.detStream.unpipe(this.recStream);
  }

  start(stream){
      console.log("starting ===== sending")
      console.log(stream)
      var dd = this.recStream;
      stream.pipe(dd);  
      var writer = this.wavWritter
      /*
      this.recStream.pipe(request.post(
      {
      'url'     : 'https://api.wit.ai/speech?client=chromium&lang=en-us&output=json',
      'headers' : {
        'Accept'        : 'application/vnd.wit.20160202+json',
        'Authorization' : 'Bearer ' + this.witToken,
        'Content-Type'  : 'audio/wav'
      }},
      function optionalCallback(err, httpResponse, body) {
        if (err) {
          return console.error('upload failed:', err);
        }
        console.log('Upload successful!  Server responded with:', body);
      })
      )*/
      dd.pipe(writer)

      setTimeout(function(){
        console.log('ending stream')
         dd.end();
      },3000)

        
    
  }

}


class Stb {
  constructor(){
    this.myrec = {}//new stream.Writable();
    this.models = new Models();

    this.isEnabled = false;
    this.sendCmd = false
      
    this.RecordingObj = null

    this.showres = function(d){
        console.log(d)
    }
 
   this.models.add({
      file: 'resources/alexa.umdl',
      sensitivity: '0.5',
      hotwords : 'snowboy'
    });

    this.detector = new Detector({
      resource: "resources/common.res",
      models: this.models,
      audioGain: 2.0
    });

    this.detector.on('silence', ()=>{
      // we need to stop the sending
      if(false){
        this.sendCmd=false;
        this.RecordingObj.stop(this.myrec);
        this.RecordingObj = null;

      }
      

    });

    this.detector.on('sound', function () {
      console.log('sound');
    });

    this.detector.on('error', function () {
      console.log('error');
    });

    this.detector.on('hotword', (index, hotword)=> {
      console.log('hotword', index, hotword);
      console.log(this.sendCmd)
      if(!this.sendCmd){
          this.sendCmd=true;
          this.RecordingObj = new RecognitionAI();
          this.RecordingObj.start(this.myrec);
      }
      

     });

  }



  start(){ 
    this.myrec = record.start({ threshold: 0, verbose : true})
    this.myrec.pipe(this.detector)

  }

}



var dd = new Stb()
dd.start()