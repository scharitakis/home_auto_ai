//microdetect-broadlink1.js


var micStream = require('node-record-lpcm16');
const {Detector, Models} = require('snowboy');
var fs = require('fs');
var stream = require('stream');
var request = require('request');
var beep = require('beepbeep');
var say = require('./mimic-node');
var adapt = require('./adapt-node');

var speechApi = require('./microsoft-speech');
var async = require('async');


var BroadlinkService = require('./node_broadlink/broadlinkService')
var repl = require("repl");





var myfile  = null;
var streamMic = null;





class Stb {
  constructor(){
      this.streamMic =null
      this.broadlink = new BroadlinkService();
      //this.broadlink.start();
      this.adapt = new adapt();
      this.adapt.on("data",(data)=>{this.processCmd1(data)});
      this.speechToText = "";

  }

  createDetector(){
  	var models = new Models();

   	models.add({
      file: 'resources/Hey_Atom.pmdl',
      sensitivity: '0.45',
      hotwords : 'hey Atom'
    });

  	var detector = new Detector({
      resource: "resources/common.res",
      models: models,
      audioGain: 1.9
    });

    detector.on('silence', ()=>{
     //console.log('silence');
    });

    detector.on('sound', function () {
      //console.log('sound');
    });

    detector.on('error', function () {
      console.log('error');
    });

    detector.on('hotword', (index, hotword)=> {
      console.log('hotword', index, hotword);
      this.stopReco();
      setTimeout(()=>{
      	//console.log('start rec======')
      	beep()
  		this.sendReco()
        },100)

        setTimeout(()=>{
        	//console.log('stop rec ======')
  		beep()
  		this.stopReco()
        },4000)
        setTimeout(()=>{
        	console.log('start detect======')
  		this.start()
        },6100)
      
      /*setTimeout(()=>{
      	this.stopReco()
      },6000)

      setTimeout(()=>{
      	this.start()
      },7000)*/
    
     });

    return detector
  }

  stopReco(){
  	micStream.stop()
  	this.streamMic.emit('end')
    this.streamMic.unpipe()

  }

  TvIntent_old(data){
  	/*
.require("TvKeyword")\
    .optionally("TvVerb")\
    .optionally("TvCommands")\
    .optionally("TvSubCommands")\
    .optionally("TvLocation")\
  	*/
  		var AplianceVerb = data.AplianceVerb
  		var ApliancesStates = "";
  		if(AplianceVerb == "turn" || AplianceVerb == "switch"){
			ApliancesStates == (data.ApliancesStates==="on")?true:false
  		}
  		if(AplianceVerb == "open" ){
  			ApliancesStates = true	
  		}
  		if(AplianceVerb == "open" ){
  			ApliancesStates = false	
  		}

  		console.log(AplianceVerb +" "+ApliancesStates)

  		switch(data.AplianceKeyword){
  			case "tv":
  				this.broadlink.devices["46-203-251-13-67-180"].send_data(0)
  				this.speak("tv")
  				break
  			case "lights":
  				this.broadlink.devices["46-203-251-13-67-180"].send_data(0)
  				this.speak("lights")
  				break
  			default:
  				this.speak("Could not "+AplianceVerb+' the '+data.AplianceKeyword)
  				break
  		}

  }

   TvIntent(data){
  	/*
.require("TvKeyword")\
    .optionally("TvVerb")\
    .optionally("TvCommands")\
    .optionally("TvSubCommands")\
    .optionally("TvLocation")\
  	*/
  		var TvVerb = data.TvVerb;
  		var TvCommand = data.TvCommands;
  		var TvSubCommand = data.TvSubCommands;
  		var TvLocation = data.TvLocation;
  		var deviceName = 'tv_'+TvLocation;
  		var cmd = null;
  		
  		if(TvVerb === "open" ){
			TvCommand = "on"
  		}
  		if(TvVerb === "close" ){
			TvCommand = "off"
  		}

  		cmd = TvCommand;

  		if(TvSubCommand!==undefined){
  			cmd = {}
  			cmd[TvSubCommand]=TvCommand
  		}



  		var command = {
  			device:deviceName,
			command:cmd
		}

		console.log('sending Command',JSON.stringify(command))

  		this.broadlink.devices["46-203-251-13-67-180"].send_data(command)
		this.speak("tv")

  }

  LightIntent(data){
  	/*
. IntentBuilder("LightIntent")\
    .require("LightKeyword")\
    .optionally("LightVerb")\
    .optionally("LightCommand")\
    .optionally("LightLocation")\
    .optionally("LightNumber")\
  	*/
  		var LightVerb = data.LightVerb;
  		var LightCommand = data.LightCommand;
  		var LightLocation = data.LightLocation;
  		var LightNumber = data.LightNumber;
  		var LightKeyword = data.LightKeyword;
  		if(LightKeyword=="light")
  		{
	  		var deviceName = 'light_'+LightNumber+'_'+LightLocation;
	  		var cmd = null;
	  		
	  		if(LightVerb === "open" ){
				LightCommand = "on"
	  		}
	  		if(LightVerb === "close" ){
				LightCommand = "off"
	  		}

	  		cmd = LightCommand;

	  		var command = {
	  			device:deviceName,
				command:cmd
			}

			console.log('sending Command',JSON.stringify(command))

	  		this.broadlink.devices["46-203-251-13-67-180"].send_data(command)
			this.speak("light")
		}else{
			//issue a group command delayed by ms

			if(LightVerb === "open" ){
				LightCommand = "on"
	  		}
	  		if(LightVerb === "close" ){
				LightCommand = "off"
	  		}
			var deviceControllersbyGroup = this.broadlink.getDevicesByGroup('lights');
			var deviceNames = Object.keys(deviceControllersbyGroup);
			var commandsToBeProcessed = []
			deviceNames.forEach((dname)=>{

				var controllers = deviceControllersbyGroup[dname] 
				controllers.forEach((cName)=>{
					var cmd = {
							device:cName,
							command:LightCommand
							}
					var broadlink = this.broadlink
					var cfn = function(cmd){
						return function(clb){
							broadlink.devices[dname].send_data(cmd)
							setTimeout(function(){clb()},400)
						}
					}(cmd)

					commandsToBeProcessed.push(cfn)
				})

				
			})
			console.log(commandsToBeProcessed);
			if(commandsToBeProcessed.length){
				async.waterfall(commandsToBeProcessed, function (err, result) {
				    // result now equals 'done'
				    console.log('lights finished')
				});
			}else{
				console.log('no group commands')
			}

		}

  }

  WeatherIntent(data){
  		var AplianceVerb = data.AplianceVerb
  		var ApliancesStates = "";
  		if(AplianceVerb == "turn" || AplianceVerb == "switch"){
			ApliancesStates == (data.ApliancesStates==="on")?true:false
  		}
  		if(AplianceVerb == "open" ){
  			ApliancesStates = true	
  		}
  		if(AplianceVerb == "open" ){
  			ApliancesStates = false	
  		}

  		console.log(AplianceVerb +" "+ApliancesStates)

  		switch(data.AplianceKeyword){
  			case "tv":
  				this.broadlink.devices["192.168.1.5"].send_data(0)
  				this.speak("tv")
  				break
  			case "lights":
  				this.broadlink.devices["192.168.1.5"].send_data(0)
  				this.speak("lights")
  				break
  			default:
  				this.speak("Could not "+AplianceVerb+' the '+data.AplianceKeyword)
  				break
  		}

  }

  speak(text){
  	 say.speak(text);
  }

  processCmd1(data){
  	//"confidence": 0.75, "target": null, "intent_type": "AplianceIntent", 
  	//"AplianceVerb": "turn", "AplianceKeyword": "tv", "ApliancesStates": "off"}
  	
  	if(data.length){
  		var pData = JSON.parse(data);
	  	console.log(pData)
	  	if(pData.intent_type!=undefined){
	  		//found intent 
		  	switch(pData.intent_type){
		  		case "TvIntent":
		  			this.TvIntent(pData);
		  			break;
		  		case "LightIntent":
		  			this.LightIntent(pData);
		  			break;
		  		default:
		  			console.log('no intent')
		  			break
		  	}
  		}
  	}else{
  		var textres = ""
  		var text = this.speechToText
  		switch(text){
			case "what is your name?":
				textres = 'My name is Alexa';
				break;
			case "how are you?":
				textres = 'I am fine thank you master';
				break;
			case "who am i?":
				textres = 'You are Stavros the master';
				break;
			case "tell me the time.":  
			case "what's the time?":  
			case "what time is it?":
				var date =new Date();
				var time = (date.getHours()>12?date.getHours()-12:date.getHours()) + ' and ' + date.getMinutes() + ' minutes';
				textres = 'The time is' +time;
			break;
			default:
				textres ='Sorry did not understand master';
				break;
		}
		this.speak(textres);


  	}
  	
   
  }

  processCmd(text){
  	var textres = ""
  	console.log(text)
  	switch(text.toLowerCase()){
  		case "what is your name?":
  			textres = 'My name is Alexa';
  			break;
      case "how are you?":
        textres = 'I am fine thank you master';
        break;
      case "who am i?":
        textres = 'You are Stavros the master';
        break;
      case "turn tv on.":
      case "turn tv off.":
      case "turn tv.":
      case "television.":
      case "switch tv on.":
      case "switch the tv on.":
      case "switch tv off.":
      case "switch the tv off.":
      case "tv.":
          this.broadlink.devices["192.168.1.5"].send_data(0)
        textres = 'TV  ';
        break;

      case "tell me the time.":  
      case "what's the time?":  
  		case "what time is it?":
  			var date =new Date();
  			var time = (date.getHours()>12?date.getHours()-12:date.getHours()) + ' and ' + date.getMinutes() + ' minutes';
  			textres = 'The time is' +time;
  			break;
      case 'switch of the lights.':  
  		case "turn off the lights.":
  			textres = 'turning of the lights master';
  			break;
  		default:
        textres ='Sorry did not understand master';
  	}
    //say.speak(textres,'Victoria');
    say.speak(textres);
  }

  sendReco1(){
  	// this is for wit.ai
  	this.streamMic =null
    this.streamMic = micStream.start({ threshold: 0, verbose : false})
  	var witToken = "2XQOXTT3HDOXGRR7BZP3Q5QMKFL4JC2D"

    var myfile = fs.createWriteStream('alexa1.wav',{encoding:'binary'})
  	// this.streamMic.pipe(myfile)

  	this.streamMic.pipe(myfile)
	this.streamMic.pipe(request.post(
      {
      'url'     : 'https://api.wit.ai/speech?v=20160526',//'https://api.wit.ai/speech?client=chromium&lang=en-us&output=json',
      'headers' : {
        'Accept'        : 'application/vnd.wit.20160202+json',
        'Authorization' : 'Bearer ' + witToken,
        'Content-Type'  : 'audio/wav'
      }},
      (err, httpResponse, body) =>{
        if (err) {
          return console.error('upload failed:', err);
        }
        console.log('Upload successful!  Server responded with:', body);
        var res = JSON.parse(body)
        this.processCmd(res._text)
      }))
  }

  sendReco(){
  	// this is for Microsoft cognitive services
  	this.streamMic =null
    this.streamMic = micStream.start({ threshold: 0, verbose : false})

	//var myfile = fs.createWriteStream('alexa1.wav',{encoding:'binary'})
	//this.streamMic.pipe(myfile)
	
	
  	speechApi.getTextFromAudioStream(this.streamMic).then((res)=>{
  		if(res!=undefined){
  			//this.processCmd(res)
  			var txtRec = res.toLowerCase()
  			this.speechToText = txtRec
  			console.log(txtRec)
  			this.adapt.parse(txtRec);
  		}else{
  			this.speechToText = ""
  			console.log('error nothing was recognized')
  		}
  	}).catch((err)=>{
  		console.log(err)
  	})
  	

  }

  start(){ 
  	this.streamMic =null
    this.streamMic = micStream.start({ threshold: 0, verbose : false})
    this.detector = this.createDetector();
    this.streamMic.pipe(this.detector)



  }

}

var app = new Stb();
app.start()

var replServer = repl.start({
	prompt: "home-auto > ",
});
replServer.context.server = app;

/*
setTimeout(function(){
	startStream('test_wav1');
},200);

setTimeout(function(){
	console.log('end test1')
	micStream.stop()
	//streamMic.emit('end')
	//streamMic.unpipe()
	//myfile.emit('end')
	streamMic.emit('end')
	streamMic.unpipe()
	//myfile.emit('end')

},3000);



setTimeout(function(){
	startStream('test_wav2');
},4000);

setTimeout(function(){
	console.log('end test2')
	micStream.stop()
	//streamMic.emit('end')
	//streamMic.unpipe()
	//myfile.emit('end')
	streamMic.emit('end')
	streamMic.unpipe()
	//myfile.emit('end')

},9000);

setTimeout(function(){
	console.log('finished')
},10000);

*/
