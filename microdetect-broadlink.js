
var micStream = require('node-record-lpcm16');
const {Detector, Models} = require('snowboy');
var fs = require('fs');
var stream = require('stream');
var request = require('request');
var beep = require('beepbeep');
var say = require('./mimic-node');
//var adapt = require('./adapt-node');

var BroadlinkService = require('../node_broadlink/broadlinkService')


var myfile  = null;
var streamMic = null;





class Stb {
  constructor(){
      this.streamMic =null
      this.broadlink = new BroadlinkService();
      this.broadlink.start();
      //this.adapt = new adapt()

  }

  createDetector(){
  	var models = new Models();

   	models.add({
      file: 'resources/alexa.umdl',
      sensitivity: '0.5',
      hotwords : 'snowboy'
    });

  	var detector = new Detector({
      resource: "resources/common.res",
      models: models,
      audioGain: 2.0
    });

    detector.on('silence', ()=>{
     //console.log('silence');
    });

    detector.on('sound', function () {
      //console.log('sound');
    });

    detector.on('error', function () {
      console.log('error');
    });

    detector.on('hotword', (index, hotword)=> {
      console.log('hotword', index, hotword);
      this.stopReco();
      setTimeout(()=>{
      	//console.log('start rec======')
      	beep()
  		this.sendReco()
        },100)

        setTimeout(()=>{
        	//console.log('stop rec ======')
  		beep()
  		this.stopReco()
        },4000)
        setTimeout(()=>{
        	console.log('start detect======')
  		this.start()
        },6100)
      
      /*setTimeout(()=>{
      	this.stopReco()
      },6000)

      setTimeout(()=>{
      	this.start()
      },7000)*/
    
     });

    return detector
  }

  stopReco(){
  	micStream.stop()
  	this.streamMic.emit('end')
    this.streamMic.unpipe()

  }

  processCmd(text){
  	var textres = ""
  	switch(text){
  		case "what is your name":
  			textres = 'My name is Alexa';
  			break;
      case "how are you":
        textres = 'I am fine thank you master';
        break;
      case "who am i":
        textres = 'You are Stavros the master';
        break;
      case "turn tv on":
      case "turn tv off":
      case "turn tv":
      case "television":
      case "switch tv on":
      case "switch the tv on":
      case "switch tv off":
      case "switch the tv off":
      case "tv":
          this.broadlink.devices["192.168.1.5"].send_data(0)
        textres = 'TV  ';
        break;

      case "tell me the time":  
      case "what's the time":  
  		case "what time is it":
  			var date =new Date();
  			var time = (date.getHours()>12?date.getHours()-12:date.getHours()) + ' and ' + date.getMinutes() + ' minutes';
  			textres = 'The time is' +time;
  			break;
      case 'switch of the lights':  
  		case "turn off the lights":
  			textres = 'turning of the lights master';
  			break;
  		default:
        textres ='Sorry did not understand master';
  	}
    //say.speak(textres,'Victoria');
    say.speak(textres);
  }

  sendReco(){
  	this.streamMic =null
    this.streamMic = micStream.start({ threshold: 0, verbose : false})
  	var witToken = "2XQOXTT3HDOXGRR7BZP3Q5QMKFL4JC2D"

    var myfile = fs.createWriteStream('alexa1.wav',{encoding:'binary'})
  	// this.streamMic.pipe(myfile)

  	this.streamMic.pipe(myfile)
	this.streamMic.pipe(request.post(
      {
      'url'     : 'https://api.wit.ai/speech?v=20160526',//'https://api.wit.ai/speech?client=chromium&lang=en-us&output=json',
      'headers' : {
        'Accept'        : 'application/vnd.wit.20160202+json',
        'Authorization' : 'Bearer ' + witToken,
        'Content-Type'  : 'audio/wav'
      }},
      (err, httpResponse, body) =>{
        if (err) {
          return console.error('upload failed:', err);
        }
        console.log('Upload successful!  Server responded with:', body);
        var res = JSON.parse(body)
        this.processCmd(res._text)
      }))
  }

  start(){ 
  	this.streamMic =null
    this.streamMic = micStream.start({ threshold: 0, verbose : false})
    this.detector = this.createDetector();
    this.streamMic.pipe(this.detector)



  }

}

var app = new Stb();



app.start()

/*
setTimeout(function(){
	startStream('test_wav1');
},200);

setTimeout(function(){
	console.log('end test1')
	micStream.stop()
	//streamMic.emit('end')
	//streamMic.unpipe()
	//myfile.emit('end')
	streamMic.emit('end')
	streamMic.unpipe()
	//myfile.emit('end')

},3000);



setTimeout(function(){
	startStream('test_wav2');
},4000);

setTimeout(function(){
	console.log('end test2')
	micStream.stop()
	//streamMic.emit('end')
	//streamMic.unpipe()
	//myfile.emit('end')
	streamMic.emit('end')
	streamMic.unpipe()
	//myfile.emit('end')

},9000);

setTimeout(function(){
	console.log('finished')
},10000);

*/
