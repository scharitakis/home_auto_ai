const fs = require('fs');
var exec = require('child_process').spawn;
var stream = require('stream');

var cmd = "sox"
var cmdArgs = [
				'-d',
				'-r', 16000, 				// sample rate
				'-c', '1',                // channels
				'-e', 'signed-integer',   // sample encoding
				'-b', '16',               // precision (bits)
				'-p',                      // pipe
			];

var passStream = new stream.PassThrough();
var fileToStream = fs.createWriteStream('mywavFile.wav');
passStream.pipe(fileToStream);			
//execute the command 			
var proc = exec('sox',['-d','-p'])
var procStream = proc.stdout;
// start listening to the stream
procStream.on('data',function(data){
	console.log(data)
	passStream.write(data);
})