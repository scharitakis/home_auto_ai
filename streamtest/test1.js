'use strict';

var execa     = require('execa'),
    stream    = require('stream');
	
const spawn = require('child_process').spawn;
const execFile = require('child_process').execFile;
	
var start = function(){
	var recording = new stream.PassThrough();
	var rec = null; // Empty out possibly dead recording process
	
	 var defaults = {
		sampleRate : 16000,
		compress   : false,
		threshold  : 0.5,
		verbose    : false,
		recordProgram : 'sox'
	};
	
	var options = Object.assign(defaults, options);
	var cmd="sox"
	var cmdArgs = [
			'-d',
            '-r', 16000, 				// sample rate
            '-c', '1',                // channels
            '-e', 'signed-integer',   // sample encoding
            '-b', '16',               // precision (bits)
            //'-t', 'wavaudio default',              // audio type
            '-p',                      // pipe
            '-'
        ];
	try{	
	
		/*
		var cp = spawn(cmd, cmdArgs, { encoding: 'binary' })
		//console.log(cp)
		cp.on('error',function(e){console.log(e)});
		var rec = cp.stdout;
		
		console.time('End Recording');	
		
		*/
		
		const child = execFile(cmd, cmdArgs, (error, stdout, stderr) => {
		  if (error) {
			throw error;
		  }
		  console.log(stdout);
		});
	
	
	
	
	}catch(e){
		console.log(e)
	}
	
	/*
	// Fill recording stream with stdout
	rec.on('data', function (data) {

		console.log('Recording %d bytes', data.length);

		recording.write(new Buffer(data, 'binary')); // convert to binary buffer
	});
	
	rec.on('end', function () {

		console.timeEnd('End Recording');

		recording.end();
	});
	
	*/
	
	return recording;
	
}

start();