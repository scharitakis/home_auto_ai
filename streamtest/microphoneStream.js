const fs = require('fs');
var exec = require('child_process').spawn;
var stream = require('stream');

class micStream{
	constructor(){
		//console.log('constructor')
		this.options={}
		this.options.cmd = "sox",
		this.options.cmdArgs = [
				'-d',
				'-r', 16000, 				// sample rate
				'-c', '1',                // channels
				'-e', 'signed-integer',   // sample encoding
				'-b', '16',               // precision (bits)
				'-p',                      // pipe
			];
			
		this.passStream = new stream.PassThrough();	
		this.proc = null;
	}
	
	start(){
		this.proc = exec('sox',['-d','-p'])
		this.procStream = this.proc.stdout;
		this.procStream.on('data',(data)=>{
			console.log(data)
			this.passStream.write(data);
		})
		this.procStream.on('error',(error)=>{
			console.log(error)
		})
		return this.passStream
	}
	
	stop(){
		console.log('stoping')
		this.procStream.end()
		this.proc.kill();
		//this.passStream.end();
		return this.passStream;
	}
}

exports.micStream = function(){ return micStream}
/*
var micRecStream = new micStream();
var stream  = micRecStream.start()

var fileWav = fs.createWriteStream('mywav.wav');
stream.pipe(fileWav)

setTimeout(function(){
	console.log('stoping recording')
	micRecStream.stop();
},6000)
*/

