var fs = require('fs');
var util = require('util');
 
//process.stdin.resume();
//process.stdin.setEncoding('utf8');
  
var request = require('request');   

var micStream = require('./microphoneStream').micStream();
console.log(micStream)

//Streams required
var stream = require('stream')
var pass = stream.PassThrough;




const Writable = stream.Writable;
const Readable = stream.Readable;
const passThrough = stream.PassThrough;

class MyWritable extends Writable {
	constructor(options){
		super(options)
	}
	
	_write(chunk, encoding, callback) {
    if (chunk.toString().indexOf('a') >= 0) {
      callback(new Error('chunk is invalid'));
    } else {
      callback();
    }
  }
}

class MyReadable extends Readable {
  constructor(options) {
    // Calls the stream.Readable(options) constructor
    super(options);
  }
}

class MyPassThrough extends passThrough {
  constructor(options) {
    // Calls the stream.Readable(options) constructor
    //this.enabled = false;
	super(options);
	
  }
  
  _read(size) {
    console.log(size)
  }
  
  
  _write(chunk, encoding, callback) {
    console.log(chunk)
	//if(this.enabled){
		console.log('pushing')
		this.push(chunk)
	//}
	callback()
  }
   
}

class MyCheckStream extends passThrough {
  constructor(options) {
    // Calls the stream.Readable(options) constructor
    //this.enabled = false;
	super(options);
	
  }
  
  _read(size) {
    console.log(size)
  }
  
  
  _write(chunk, encoding, callback) {
    console.log(chunk)
	//if(this.enabled){
	console.log('checking')
	//this.push(chunk)
	//}
	callback()
  }
    
  
}


class App {
	constructor(){
		this.myWriteStream = new MyWritable();
		this.myReadStream = new MyReadable();
		this.checkStream = new MyCheckStream();
		this.myWritableFile = fs.createWriteStream('testFile3.wav',{encoding:'binary'});
	}
	
	start(){
		this.myPassStream = new MyPassThrough();
		var mic = new micStream();
		var micstream = mic.start();
		micstream.pipe(this.checkStream)
		//process.stdin.pipe(this.checkStream)
		
	}
	
	beginSending(){
		this.myPassStream = new MyPassThrough();
		process.stdin.pipe(this.myPassStream)
		var TOKEN = '2XQOXTT3HDOXGRR7BZP3Q5QMKFL4JC2D'
		
		
		
		this.myPassStream.pipe(this.myWritableFile)
		
		/*
		this.myPassStream.pipe(request.post({url:'https://api.wit.ai/speech?client=chromium&lang=en-us&output=json', 
		  headers : {
			'Accept'        : 'application/vnd.wit.20160202+json',
			'Authorization' : 'Bearer ' + TOKEN,
			'Content-Type'  : 'audio/wav',//'text/plain',
			//'Transfer-Encoding': 'chunked'
		  }
		  }, function(err,httpResponse,body){console.log(body) })
		  )

		
		/*this.myPassStream.pipe(request.post({url:'http://requestb.in/1dl91k01', 
		  headers : {
			//'Accept'        : 'application/vnd.wit.20160202+json',
			'Authorization' : 'Bearer ' + 'asdsadasd',
			'Content-Type'  : 'audio/wav',//'text/plain',
			//'Transfer-Encoding': 'chunked'
		  }
		  }, function(err,httpResponse,body){console.log(body) })
		  )*/
	}
	
	stopSend(){
		this.myPassStream.end();
	}
	
	
	stop(){
		process.stdin.unpipe(this.myPassStream)
		
	}
	
}

var myApp = new App();
myApp.start();


setTimeout(function(){
	console.log('=======beginning send');
	//myApp.stop()
	myApp.beginSending()
},100)

setTimeout(function(){
	console.log('========stop sending');
	//myApp.stop()
	myApp.stopSend()

},6000)

/*
setTimeout(function(){
	console.log('=============beginning send');
	//myApp.stop()
	myApp.beginSending()
},6000)


setTimeout(function(){
	console.log('========stop sending');
	//myApp.stop()
	myApp.stopSend()

},9000)

/*
setTimeout(function(){
	console.log('enable stream');
	//myApp.stop()
	myApp.enable()
},9000)

setTimeout(function(){
	console.log('disable stream');
	myApp.stop()
	//myApp.disable()
},12000)
*/

